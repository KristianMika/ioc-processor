# IOC Processor

My solution to a programming assignment for Python Developer position @ redamp.cz.

# Install:
	git clone git@gitlab.com:KristianMika/ioc-processor.git
	cd ioc-processor/
	pip3 install -r requirements.txt
	chmod +x ioc_processor/processor.py
	sudo ln -s $PWD/ioc-processor/processor.py /usr/local/sbin/ioc_processor

# Usage
	usage: ioc_processor.py [-h] [-v] [-e] [-t] [-H HOST] [-p PORT] [-d DATABASE]
                    [-u USER] [-s SEPARATOR] [-f FILE]
                    [urls [urls ...]]

	positional arguments:
	  urls                  specify urls of files with IOCs

	optional arguments:
	  -h, --help            show this help message and exit
	  -v, --verbose         enable verbose mode
	  -e, --essential       enable essential mode - stores only ips and urls
	  -t, --tables          creates tables that are required for ioc_processor
	  -H HOST, --host HOST  specify database host
	  -p PORT, --port PORT  specify database port
	  -d DATABASE, --database DATABASE
	                        specify database name
	  -u USER, --user USER  specify database user
	  -s SEPARATOR, --separator SEPARATOR
	                        specify separator
	  -f FILE, --file FILE  specify a file with urls

