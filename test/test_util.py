import json
import os
import sys
import unittest
from unittest.mock import patch

# an awful way of importing files from other directories
curr_dir = (os.path.dirname(os.path.realpath(__file__)))
sys.path.append(os.path.join(curr_dir, '..', 'ioc_processor'))
import util


class TestProcessor(unittest.TestCase):

    def test_read_read_db_credentials(self):
        test_credentials = {'host': 'griffin',
                            'port': 101,
                            'database': 'test_db',
                            'user': 'user_name'
                            }

        cred_file_name = 'test_cred.json'
        with open(cred_file_name, 'w') as file:
            json.dump(test_credentials, file)

        creds = util.read_db_credentials(cred_file_name)

        os.remove(cred_file_name)

        self.assertEqual(test_credentials, creds)

    def test_store_db_credentials(self):
        test_credentials = {'host': 'griffin',
                            'port': 101,
                            'database': 'test_db',
                            'user': 'user_name',
                            'password': 'secure_password123'
                            }

        cred_file_name = 'test_cred.json'
        util.store_db_credentials(test_credentials, cred_file_name)

        read_creds = ''
        with open(cred_file_name, 'r') as file:
            read_creds = json.load(file)

        os.remove(cred_file_name)
        del test_credentials['password']

        self.assertEqual(read_creds, test_credentials)

    @patch('builtins.input', lambda user_input: 'test_db')
    def test_load_missing_arguments(self):
        # Missing db_name and port
        test_credentials = {'host': 'griffin',
                            'user': 'user_name',
                            'password': 'secure_password123'
                            }

        missing_db_name = "test_db"

        all_creds = util.load_missing_arguments(test_credentials)

        # add missing values
        test_credentials['database'] = missing_db_name
        test_credentials['port'] = 5432

        self.assertEqual(test_credentials, all_creds)


if __name__ == '__main__':
    unittest.main()
