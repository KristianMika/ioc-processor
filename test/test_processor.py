import hashlib
import os
import sys
import unittest

# an awful way of importing files from other directories
curr_dir = (os.path.dirname(os.path.realpath(__file__)))
sys.path.append(os.path.join(curr_dir, '..', 'ioc_processor'))

import config
import processor
import util
import db_session


class TestProcessor(unittest.TestCase):
    # Test login credentials
    credentials = {"host": "127.0.0.1",
                   "port": 5432,
                   "database": "ioc_db",
                   "user": "ioc_processor",
                   "password": "O5kHwj8#Mqh@U4O^"}
    RUN_TESTS_WITH_DB = False

    def test_get_ioc_format(self):
        config.TESTING = True
        proc = processor.Processor(None, False)

        ip_ioc = '190.122.109.224'
        ip_ioc_format = proc.__get_ioc_format__(ip_ioc, None)
        self.assertEqual(ip_ioc_format, ['ip'])

        longer_ioc = '2.81.219.150#4#3#Malicious Host#PT#Braga#41.5503005981,-8.4201002121#3'
        longer_ioc_format = proc.__get_ioc_format__(longer_ioc, '#')
        self.assertEqual(longer_ioc_format, ['ip', 'other_ioc', 'other_ioc', 'other_ioc',
                                             'other_ioc', 'other_ioc', 'other_ioc', 'other_ioc'])

        longer_ioc = '4#2.81.219.150#3#Malicious Host#PT#Braga#41.5503005981,-8.4201002121#3'
        longer_ioc_format = proc.__get_ioc_format__(longer_ioc, '#')
        self.assertEqual(longer_ioc_format, ['other_ioc', 'ip', 'other_ioc', 'other_ioc', 'other_ioc',
                                             'other_ioc', 'other_ioc', 'other_ioc'])

        url_ioc = 'https://www.janamarbles.com/images/oi/LINKEDIN'
        url_ioc_format = proc.__get_ioc_format__(url_ioc, None)
        self.assertEqual(url_ioc_format, ['url'])

        url_ioc = 'http://202.182.112.26/ap/signin?key=a@b.c&openid.assoc_handle=' \
                  'jpflex&openid.claimed_id=http://specs.openid.net/auth/2.0/identifier' \
                  '_select&openid.identity=http://specs.openid.net/auth/2.0/identifier_select&' \
                  'openid.mode=checkid_setup&openid.ns=http://specs.openid.net/auth/2.0&openid.pape.' \
                  'max_auth_age=0&openid.return_to=https://www.amazon.co.jp/?ref_%3Dnav_em_hd_re_signin&ref_=' \
                  'nav_em_hd_clc_signin'
        url_ioc_format = proc.__get_ioc_format__(url_ioc, None)
        self.assertEqual(url_ioc_format, ['url'])

    def test_download_content(self):
        config.TESTING = True
        proc = processor.Processor(None, False)

        url_1 = 'http://reputation.alienvault.com/reputation.data'
        content_1 = '\n'.join(proc.__download_content__(url_1)) + '\n'
        hash_1 = hashlib.sha256(content_1.encode()).hexdigest()
        self.assertEqual(hash_1, '30bfe15d4d605e0f78cb2af170fcd64e5ca2809802dd57e9c193b549ee15f53e')

    def test_process(self):
        # Current environment for integration tests doesn't have postresql
        if not TestProcessor.RUN_TESTS_WITH_DB:
            return

        session = db_session.DbSession(TestProcessor.credentials)
        session.clean_db()
        proc = processor.Processor(session, False)
        url = 'http://reputation.alienvault.com/reputation.data'

        proc.process(url, '#')
        ip_count = 275630
        ip_count_from_db = session.test_execute_query('SELECT COUNT(ip) from ips;')
        ip_count_from_db = util.extract_fst_elem(ip_count_from_db)

        self.assertEqual(ip_count, ip_count_from_db)


if __name__ == '__main__':
    unittest.main()
