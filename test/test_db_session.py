import os
import sys
import unittest

# an awful way of importing files from other directories
curr_dir = (os.path.dirname(os.path.realpath(__file__)))
sys.path.append(os.path.join(curr_dir, '..', 'ioc_processor'))

import db_session
import util


class TestDbSession(unittest.TestCase):
    # Test login credentials
    credentials = {"host": "127.0.0.1",
                   "port": 5432,
                   "database": "ioc_db",
                   "user": "ioc_processor",
                   "password": "O5kHwj8#Mqh@U4O^"}

    def test_insert_origin(self):
        session = db_session.DbSession(TestDbSession.credentials)
        session.clean_db()

        origin = 'http://reputation.alienvault.com/reputation.data'
        origin_id = util.extract_fst_elem(session.insert_origin(origin))

        self.assertGreater(origin_id, 0)

        origin_count = session.test_execute_query("SELECT COUNT(*) FROM origins;")
        origin_count = util.extract_fst_elem(origin_count)
        self.assertEqual(1, origin_count)

        origin_from_db = util.extract_fst_elem(session.test_execute_query("SELECT origin FROM origins;"))
        self.assertEqual(origin, origin_from_db)

        origin2 = 'http://reputation.alienvault.com/'
        origin2_id = util.extract_fst_elem(session.insert_origin(origin2))

        self.assertGreater(origin2_id, 0)

        origin_count = session.test_execute_query("SELECT COUNT(*) FROM origins;")
        origin_count = util.extract_fst_elem(origin_count)
        self.assertEqual(2, origin_count)

    def test_insert(self):
        session = db_session.DbSession(TestDbSession.credentials)
        session.clean_db()

        origin = 'http://reputation.alienvault.com/reputation.data'
        ip = '127.0.0.1'
        url = 'https://secure.runescape.com-zx.ru/m=weblogin/loginform419,851,105,41664313,1'
        other_ioc = '10:00:19'

        origin_id = util.extract_fst_elem(session.insert_origin(origin))

        session.insert(origin_id, [(0, ip)], [(1, url)], [(2, other_ioc)])

        db_res = session.test_execute_query('SELECT ip, url, ioc '
                                            'FROM ips '
                                            'JOIN urls ON ips.set_id = urls.set_id '
                                            'JOIN ioc_instances ON ips.set_id = ioc_instances.set_id '
                                            'JOIN other_iocs ON ioc_instances.ioc_id = other_iocs.id;')

        self.assertEqual((ip, url, other_ioc), db_res)


if __name__ == '__main__':
    unittest.main()
