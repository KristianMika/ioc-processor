from pathlib import Path
import os

"""
This file contains global variables that are used by all modules.
"""

# if verbose mode is selected, print_verbose will be set to the print function
print_verbose = lambda *_: None

PROCESS_FOLDER = os.path.join(str(Path.home()), '.ioc_processor')
CRED_FILE = '.credentials.json'
CRED_FILE_PATH = os.path.join(PROCESS_FOLDER, CRED_FILE)

ESSENTIAL_CREDENTIALS = ['host', 'database', 'user', 'password']
UNESSENTIAL_CREDENTIALS = ['port']
ALL_CREDENTIALS = ESSENTIAL_CREDENTIALS + UNESSENTIAL_CREDENTIALS

DEFAULT_PORT = 5432

TESTING = False
