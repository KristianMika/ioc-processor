import argparse
import json
import os
import sys

import config


def parse_args():
    """
    Parses CL arguments.
    :return: parsed arguments as a namespace object
    """

    # Password can not be specified as an CL argument due to security reasons
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", action='store_true', help="enable verbose mode")
    parser.add_argument('-e', '--essential', action='store_true',
                        help='enable essential mode - store only ips and urls')
    parser.add_argument('-t', '--tables', action='store_true',
                        help='create tables that are required for ioc_processor')

    parser.add_argument('-H', "--host", type=str, help="specify database host")
    parser.add_argument('-p', "--port", type=int, help="specify database port")
    parser.add_argument('-d', "--database", type=str, help="specify database name")
    parser.add_argument('-u', "--user", type=str, help="specify database user")
    parser.add_argument('-s', '--separator', type=str, help='specify separator')
    parser.add_argument('-f', '--file', type=str, help='specify a file with urls')

    parser.add_argument("urls", nargs='*', type=str, help="specify urls of files with IOCs")

    arguments = parser.parse_args()
    if not arguments.urls and not arguments.file and not arguments.tables:
        sys.stderr.write("You must provide at least on of these: urls, file, tables\n")
        return

    return arguments


def create_cred_dict_from_namespace(cl_args):
    """
    Takes credentials from an argument namespace object
     returned by argparse and stores them into a dictionary

    :param cl_args: a namespace object with all CL arguments
    :return: login credentials and DB specifications as a dictionary
    """
    credentials = {}
    cl_args_dict = vars(cl_args)

    for key in config.ALL_CREDENTIALS:
        if key in cl_args_dict and cl_args_dict[key]:
            credentials[key] = cl_args_dict[key]

    return credentials


def read_db_credentials(json_file):
    """
    Reads db login credentials from the 'json_file' file.

    :param json_file: credential file
    :return: dictionary with loaded credentials
    """

    if not os.path.isfile(json_file):
        return {}

    config.print_verbose("Reading db credentials from a file... ")
    with open(json_file, 'r') as file:
        credentials = json.load(file)

    config.print_verbose("Done")
    return credentials


def load_missing_arguments(credentials):
    """
    Loads essential login credentials that were not specified as CL arguments,
     nor were present in the 'config.CRED_FILE_PATH' file from the previous use.

    :param credentials: dictionary with credentials
    :return: an updated and complete credential dictionary
    """
    credentials = credentials.copy()
    for field in config.ESSENTIAL_CREDENTIALS:
        if field not in credentials:
            credentials[field] = input("{}: ".format(field))

    # Set default port
    if not 'port' in credentials:
        credentials['port'] = config.DEFAULT_PORT

    return credentials


def store_db_credentials(credentials, json_file):
    """
    Stores a credential dictionary into the 'json_file' file.

    :param credentials: a dictionary with credentials and db specifications
    :param json_file: name of the output file
    :return:  None
    """
    config.print_verbose("Storing DB credentials into a file... ")
    credentials = credentials.copy()

    # Remove password from credentials
    if 'password' in credentials:
        del credentials['password']

    with open(json_file, 'w') as file:
        json.dump(credentials, file)

    config.print_verbose('Done')


def read_urls_from_file(file_name):
    """
    Loads urls of files with IOCs from 'file_name'

    :param file_name: file with urls
    :return: list of urls
    """

    if not os.path.isfile(file_name):
        return []

    with open(file_name, 'r') as file:
        urls = file.read().splitlines()

    return urls


def extract_fst_elem(in_tuple):
    """
    Returns the value at index 0 if 'in_tuple' is not None;

    :param in_tuple: tuple
    :return:
    """
    return in_tuple[0] if in_tuple else in_tuple


def create_proc_folder():
    """
    Creates folder for storing login db credentials.
    :return: None
    """
    if not os.path.isdir(config.PROCESS_FOLDER):
        os.makedirs(config.PROCESS_FOLDER)
