import sys

import config
from psycopg2 import connect, OperationalError, errors


class DbSession:
    """
    Class DbSession represents a single connection to database.
    Its main job is to interact with database and store IOCs.
    """

    INSERT_IOC_SET_QUERY = 'INSERT INTO ioc_sets(origin_id) VALUES (%s) RETURNING id;'
    SELECT_ORIGIN_ID_QUERY = 'SELECT id  FROM origins WHERE origin = %s;'
    SELECT_IOC_ID_QUERY = 'SELECT id FROM other_iocs WHERE ioc = %s;'
    INSERT_ORIGIN_QUERY = 'INSERT INTO origins(origin) VALUES (%s) RETURNING id;'
    INSERT_URL_QUERY = 'INSERT INTO urls(set_id, pos_index, url) VALUES (%s, %s, %s);'
    INSERT_IP_QUERY = 'INSERT INTO ips(set_id, pos_index, ip) VALUES (%s, %s, %s);'
    INSERT_OTHER_IOC_QUERY = 'INSERT INTO other_iocs(ioc) VALUES(%s) ON CONFLICT DO NOTHING RETURNING id;'
    INSERT_IOC_INSTANCE_QUERY = 'INSERT INTO ioc_instances(set_id, ioc_id, pos_index) VALUES(%s, %s, %s);'

    TABLE_EXISTS_QUERY = 'SELECT EXISTS(SELECT * FROM information_schema.tables ' \
                         'WHERE table_name = %s and table_catalog = %s);'

    CREATE_TABLE_QUERIES = {'origins': ' CREATE TABLE ORIGINS('
                                       'id SERIAL PRIMARY KEY,'
                                       'origin TEXT NOT NULL UNIQUE);',

                            'ioc_sets': ' CREATE TABLE ioc_sets('
                                        'id SERIAL PRIMARY KEY,'
                                        'origin_id INTEGER NOT NULL REFERENCES origins(id));',

                            'urls': ' CREATE TABLE URLS('
                                    'id SERIAL PRIMARY KEY,'
                                    'set_id INTEGER NOT NULL REFERENCES ioc_sets(id),'
                                    'pos_index INTEGER,'
                                    'url TEXT NOT NULL,'
                                    'UNIQUE(set_id, pos_index, url));',

                            'ips': 'CREATE TABLE IPS('
                                   'id SERIAL PRIMARY KEY,'
                                   'set_id INTEGER NOT NULL REFERENCES ioc_sets(id),'
                                   'pos_index INTEGER,'
                                   'ip TEXT NOT NULL,'
                                   'UNIQUE(set_id, pos_index, ip));',

                            'other_iocs': ' CREATE TABLE OTHER_IOCS('
                                          'id SERIAL PRIMARY KEY,'
                                          'ioc TEXT NOT NULL UNIQUE);',

                            'ioc_instances': ' CREATE TABLE IOC_INSTANCES('
                                             'id SERIAL PRIMARY KEY,'
                                             'set_id INTEGER NOT NULL references ioc_sets(id),'
                                             'ioc_id INTEGER NOT NULL references other_iocs(id),'
                                             'pos_index INTEGER,'
                                             'UNIQUE(set_id, ioc_id, pos_index));'
                            }

    def __init__(self, credentials):
        """
        The constructor connects to database and stores the connection object as well as a cursor object
        :param credentials: a dictionary with login credentials and db login specifications
        """
        config.print_verbose('Connecting to database...')

        try:
            self.connection = connect(**credentials)

        except OperationalError:

            # Hide password
            credentials['password'] = "*" * len(credentials['password'])
            sys.stderr.write('Unable to connect to database:\n'
                             'A connection attempt has been made using these credentials:\n'
                             '{}\n'.format(credentials))

            raise ValueError()

        self.cursor = self.connection.cursor()
        self.database = credentials['database']

        config.print_verbose('Done')

    def create_tables(self):
        """
        Creates all tables that are necessary for storing IOCs.
        :return: None
        """

        for table in DbSession.CREATE_TABLE_QUERIES.keys():
            config.print_verbose("Creating table {}.".format(table))

            self.cursor.execute(DbSession.TABLE_EXISTS_QUERY, [table, self.database])

            tmp = self.cursor.fetchone()[0]
            if not tmp:
                self.cursor.execute(DbSession.CREATE_TABLE_QUERIES[table])
            else:
                config.print_verbose("Table {} already exists.".format(table))

        self.commit()
        config.print_verbose('Tables have been successfully created.')

    def insert_origin(self, origin):
        """
        Stores the origin into database and returns origins(id).

        :param origin: origin of the IOCs
        :return: origin_id
        """

        # Get the origin_id if exists
        origin_id = 0
        try:
            self.cursor.execute(DbSession.SELECT_ORIGIN_ID_QUERY, [origin])
            origin_id = self.cursor.fetchone()
        except errors.UndefinedTable:
            sys.stderr.write("One or more tables are missing.\n"
                             "Run with --tables flag to create missing tables.\n")
            # There is nothing to clean up, terminate
            exit(1)

        # The origin is not present, insert it and acquire id
        if not origin_id:
            self.cursor.execute(DbSession.INSERT_ORIGIN_QUERY, [origin])
            origin_id = self.cursor.fetchone()

        return origin_id

    def commit(self):
        """
        Used after a file with urls has been processed to commit changes.
        :return: None
        """
        config.print_verbose("Committing changes...")
        self.connection.commit()
        config.print_verbose("Done")

    def insert(self, origin_id, ip=None, url=None, other_ioc=None):
        """
        Stores a set of IOCs into a database

        :param origin_id: Id of the origin of the IOC
        :param ip: pairs of indices and ip addresses that are a part of an IOC set
        :param url: pairs of indices and urls that are a part of an IOC set
        :param other_ioc: pairs of indices and iocs that are not ip addresses, nor urls
        :return: None
        """

        self.cursor.execute(DbSession.INSERT_IOC_SET_QUERY, [origin_id])
        ioc_set_id = self.cursor.fetchone()

        if url:
            for (index, url) in url:
                self.cursor.execute(DbSession.INSERT_URL_QUERY, [ioc_set_id, index, url])

        if ip:
            for (index, ip) in ip:
                self.cursor.execute(DbSession.INSERT_IP_QUERY, [ioc_set_id, index, ip])

        if other_ioc:
            for (index, ioc) in other_ioc:
                self.cursor.execute(DbSession.INSERT_OTHER_IOC_QUERY, [ioc])
                ioc_id = self.cursor.fetchone()

                if not ioc_id:
                    self.cursor.execute(DbSession.SELECT_IOC_ID_QUERY, [ioc])
                    ioc_id = self.cursor.fetchone()
                self.cursor.execute(DbSession.INSERT_IOC_INSTANCE_QUERY, [ioc_set_id, ioc_id, index])

    def clean_db(self):
        """ Method for testing - erases all tables """

        config.print_verbose('Deleting tables...')
        self.cursor.execute('delete from urls;'
                            'delete from ips;'
                            'delete from ioc_instances;'
                            'delete from other_iocs;'
                            'delete from ioc_sets;'
                            'delete from origins;')
        self.connection.commit()

    def test_execute_query(self, query):
        """
        For testing purposes

        :param query: query to be executed
        :return: query result
        """
        self.cursor.execute(query)
        return self.cursor.fetchone()

    def __del__(self):
        """ Destructor's job is to close connection. """

        config.print_verbose('Disconnecting from database...')

        if 'cursor' in locals():
            self.cursor.close()
        if 'connection' in locals():
            self.connection.close()

        config.print_verbose('Done')
