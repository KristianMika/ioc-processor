#!/usr/bin/python3
import re
import sys

import config
import requests
import util
from db_session import DbSession


class Processor:
    """
    The Processor class represents an IOC processor that is responsible
     for downloading IOCs and storing them into a database
    """

    # a regex string that matches urls
    # matches : http://reputation.alienvault.com/reputation.data, https://duckduckgo.com/, ...
    # doesn't match: www.website.com, website.com, website...
    REGEX_MATCH_URL = '^https?:\/\/(?:w{3}\.)?(?:[^\/]+\.?)+.+$'

    # a regex string that matches IPv4 addresses
    REGEX_MATCH_IP = '^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$'

    def __init__(self, db_session, essential):
        """
        Constructor only stores a db_session object for later interaction.

        :param db_session: object that interacts with a database
        :param essential: if true, only urls and ips are stored
        """

        if not db_session and not config.TESTING:
            raise ValueError("db_session is None")

        self.db_session = db_session
        self.essential = essential

    def process(self, url, separator):
        """
        Takes a url of a file with IOCs and a csv separator. This method downloads a file,
        determines format of IOCs and stores them into database.

        :param url: urls of the csv file
        :param separator: csv separator
        :return: None
        """

        iocs = self.__download_content__(url)
        if not iocs:
            sys.stderr.write(
                "Could not download file {}!\n"
                "Check the url and your Internet connection and try again.\n".format(url))
            return

        ioc_format = self.__get_ioc_format__(iocs[0], separator)
        config.print_verbose("Format of IOCs is: {}".format(ioc_format))

        # Get origin_id and use it for other IOCs
        origin_id = self.db_session.insert_origin(url)

        counter = 1
        config.print_verbose('Processing {}'.format(url))
        for ioc_line in iocs:

            ioc_dict = {}
            ioc_line = ioc_line.split(separator) if separator else [ioc_line]

            # from an ioc_line and ioc_format creates an ioc_dict of type : list of iocs
            for i in range(len(ioc_line)):
                if not ioc_format[i] in ioc_dict:
                    ioc_dict[ioc_format[i]] = []

                # If the essential mode is on, don't store other iocs
                if ioc_format[i] == 'other_ioc' and self.essential:
                    continue
                ioc_dict[ioc_format[i]].append((i, ioc_line[i]))

            self.db_session.insert(origin_id, **ioc_dict)

            # Prints current progress
            if counter % 1000 == 0 or counter == len(iocs):
                print("\r{:.2f}%".format(counter / len(iocs) * 100), flush=True, end='')

            counter += 1
        print()
        config.print_verbose('Done')
        self.db_session.commit()

    @staticmethod
    def __get_ioc_format__(ioc_set, separator):
        """
        get_IOC_format method uses regex expressions to identify URLs and ip addresses.
        It also assumes that all IOCs from one file are in the same format.

        :param ioc_set: one line from an input file
        :param separator: character that is used to separate values
        :return: a list of ioc types (url, ip, other_ioc)
        """

        ioc_set = ioc_set.split(separator) if separator else [ioc_set]
        ioc_format = []

        for ioc_set in ioc_set:
            if re.search(Processor.REGEX_MATCH_URL, ioc_set):
                ioc_format.append('url')

            elif re.search(Processor.REGEX_MATCH_IP, ioc_set):
                ioc_format.append('ip')

            else:
                ioc_format.append('other_ioc')

        return ioc_format

    @staticmethod
    def __download_content__(url):
        """
        Downloads a file with IOCs from the 'url' url.

        :param url: url of a file with IOCs
        :return: list of decoded IOCs
        """
        config.print_verbose('Downloading file... ')

        try:
            resp = requests.get(url)
        except requests.exceptions.ConnectionError:
            return None

        config.print_verbose('Done')
        return resp.content.decode('utf-8').splitlines()


def main():
    util.create_proc_folder()

    credentials = util.read_db_credentials(config.CRED_FILE_PATH)

    arguments = util.parse_args()

    if not arguments:
        return

    config.print_verbose = print if arguments.verbose else lambda *_: None

    credentials_from_cl = util.create_cred_dict_from_namespace(arguments)

    # merge creds, those that were passed from CL have higher priority
    credentials = {**credentials, **credentials_from_cl}

    credentials = util.load_missing_arguments(credentials)

    try:
        session = DbSession(credentials)
    except ValueError:
        return

    if arguments.tables:
        session.create_tables()

    processor = Processor(session, arguments.essential)

    urls = arguments.urls if arguments.urls else []

    if arguments.file:
        urls += util.read_urls_from_file(arguments.file)

    for url in urls:
        processor.process(url, arguments.separator)

    util.store_db_credentials(credentials, config.CRED_FILE_PATH)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('\nTerminating... ')
